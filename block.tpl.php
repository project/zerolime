<?php
// Drupal themes designed by Nemketto.com.
// Created Aug 15, 2010, Last Updated: Oct 29, 2010
?>
<div class="block block-<?php print $block->module; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>">
  <?php if ($block->subject): ?><h2 class="title"><?php print $block->subject; ?></h2><?php endif?>
  <div class="content"><?php print $block->content; ?></div>
</div>
